using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using SE_Project_Backend.models.Fantasy_Team;

namespace SE_Project_Backend.services
{
    public class FantasyTeamPersistenceService : NBAPredictorContext
    {
        public FantasyTeamPersistenceService()
        {
        }

        public FantasyTeamPersistenceService(DbContextOptions<NBAPredictorContext> options)
            : base(options)
        {
        }
        public async Task<List<FantasyTeam>> getFantasyTeams()
        {
            return await FantasyTeams.ToListAsync(); ;
        }
    }
}