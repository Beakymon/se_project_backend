﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SE_Project_Backend.models.Fantasy_Team;

#nullable disable

namespace SE_Project_Backend.services
{
    public partial class NBAPredictorContext : DbContext
    {
        public NBAPredictorContext()
        {
        }

        public NBAPredictorContext(DbContextOptions<NBAPredictorContext> options)
            : base(options)
        {
        }

        public virtual DbSet<FantasyTeam> FantasyTeams { get; set; }
        public virtual DbSet<FantasyTeamPlayer> FantasyTeamPlayers { get; set; }
        public virtual DbSet<Player> Players { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;user=root;password=stilton19;port=3306;database=NBAPredictor", Microsoft.EntityFrameworkCore.ServerVersion.Parse("8.0.28-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_0900_ai_ci");

            modelBuilder.Entity<FantasyTeam>(entity =>
            {
                entity.Property(e => e.FantasyTeamId)
                    .HasMaxLength(50)
                    .HasColumnName("fantasy_team_id");

                entity.Property(e => e.FantasyScore)
                    .HasPrecision(2, 1)
                    .HasColumnName("fantasy_score");

                entity.Property(e => e.TeamName)
                    .HasMaxLength(50)
                    .HasColumnName("team_name");
            });

            modelBuilder.Entity<FantasyTeamPlayer>(entity =>
            {
                entity.HasKey(e => new { e.FantasyTeamId, e.PlayerId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("FantasyTeam_Player");

                entity.HasIndex(e => e.PlayerId, "player_id");

                entity.Property(e => e.FantasyTeamId)
                    .HasMaxLength(50)
                    .HasColumnName("fantasy_team_id");

                entity.Property(e => e.PlayerId).HasColumnName("player_id");

                entity.HasOne(d => d.FantasyTeam)
                    .WithMany(p => p.FantasyTeamPlayers)
                    .HasForeignKey(d => d.FantasyTeamId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fantasyteam_player_ibfk_1");

                entity.HasOne(d => d.Player)
                    .WithMany(p => p.FantasyTeamPlayers)
                    .HasForeignKey(d => d.PlayerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fantasyteam_player_ibfk_2");
            });

            modelBuilder.Entity<Player>(entity =>
            {
                entity.ToTable("Player");

                entity.Property(e => e.PlayerId).HasColumnName("player_id");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Ast).HasColumnName("AST");

                entity.Property(e => e.Blk).HasColumnName("BLK");

                entity.Property(e => e.Drb)
                    .HasPrecision(2, 1)
                    .HasColumnName("DRB");

                entity.Property(e => e.EFgPercent)
                    .HasPrecision(10)
                    .HasColumnName("eFG_Percent");

                entity.Property(e => e.Fg).HasColumnName("FG");

                entity.Property(e => e.FgPercent)
                    .HasPrecision(10)
                    .HasColumnName("FG_Percent");

                entity.Property(e => e.Fga).HasColumnName("FGA");

                entity.Property(e => e.Ft)
                    .HasPrecision(2, 1)
                    .HasColumnName("FT");

                entity.Property(e => e.FtPercent)
                    .HasPrecision(10)
                    .HasColumnName("FT_Percent");

                entity.Property(e => e.Fta)
                    .HasPrecision(3, 1)
                    .HasColumnName("FTA");

                entity.Property(e => e.Gs).HasColumnName("GS");

                entity.Property(e => e.Mp).HasColumnName("MP");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.Orb).HasColumnName("ORB");

                entity.Property(e => e.Pf).HasColumnName("PF");

                entity.Property(e => e.Pos)
                    .HasMaxLength(50)
                    .HasColumnName("pos");

                entity.Property(e => e.Pts).HasColumnName("PTS");

                entity.Property(e => e.Season)
                    .HasMaxLength(50)
                    .HasColumnName("season");

                entity.Property(e => e.Stl).HasColumnName("STL");

                entity.Property(e => e.Team)
                    .HasMaxLength(50)
                    .HasColumnName("team");

                entity.Property(e => e.ThreeP).HasColumnName("Three_P");

                entity.Property(e => e.ThreePa).HasColumnName("Three_PA");

                entity.Property(e => e.Tov).HasColumnName("TOV");

                entity.Property(e => e.Trb).HasColumnName("TRB");

                entity.Property(e => e.TwoP)
                    .HasPrecision(2, 1)
                    .HasColumnName("Two_P");

                entity.Property(e => e.TwoPPercent)
                    .HasPrecision(10)
                    .HasColumnName("Two_P_Percent");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
