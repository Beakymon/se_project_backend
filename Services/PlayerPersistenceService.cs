using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Collections.Generic;
using SE_Project_Backend.models.Fantasy_Team;
namespace SE_Project_Backend.services
{
    public class PlayerPersistenceService : NBAPredictorContext
    {
        public PlayerPersistenceService()
        {
        }

        public PlayerPersistenceService(DbContextOptions<NBAPredictorContext> options)
            : base(options)
        {
        }

        public async Task<List<Player>> getAllPlayers()

        {

            return await Players.ToListAsync(); ;
        }
    }
}