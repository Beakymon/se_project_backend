﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SE_Project_Backend.models.Fantasy_Team
{
    public partial class FantasyTeamPlayer
    {
        public string FantasyTeamId { get; set; }
        public int PlayerId { get; set; }

        public virtual FantasyTeam FantasyTeam { get; set; }
        public virtual Player Player { get; set; }
    }
}
