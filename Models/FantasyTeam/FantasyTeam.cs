﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SE_Project_Backend.models.Fantasy_Team
{
    public partial class FantasyTeam
    {
        public FantasyTeam()
        {
            FantasyTeamPlayers = new HashSet<FantasyTeamPlayer>();
        }

        public string FantasyTeamId { get; set; }
        public string TeamName { get; set; }
        public decimal? FantasyScore { get; set; }

        public virtual ICollection<FantasyTeamPlayer> FantasyTeamPlayers { get; set; }
    }
}
