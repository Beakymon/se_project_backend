﻿using System;
using System.Collections.Generic;

#nullable disable

namespace SE_Project_Backend.models.Fantasy_Team
{
    public partial class Player
    {
        public Player()
        {
            FantasyTeamPlayers = new HashSet<FantasyTeamPlayer>();
        }

        public int PlayerId { get; set; }
        public string Name { get; set; }
        public string Pos { get; set; }
        public string Season { get; set; }
        public int? Age { get; set; }
        public string Team { get; set; }
        public int? G { get; set; }
        public int? Gs { get; set; }
        public float? Mp { get; set; }
        public float? Fg { get; set; }
        public float? Fga { get; set; }
        public decimal? FgPercent { get; set; }
        public float? ThreeP { get; set; }
        public float? ThreePa { get; set; }
        public decimal? TwoP { get; set; }
        public decimal? TwoPPercent { get; set; }
        public decimal? EFgPercent { get; set; }
        public decimal? Ft { get; set; }
        public decimal? Fta { get; set; }
        public decimal? FtPercent { get; set; }
        public float? Orb { get; set; }
        public decimal? Drb { get; set; }
        public float? Trb { get; set; }
        public float? Ast { get; set; }
        public float? Stl { get; set; }
        public float? Blk { get; set; }
        public float? Tov { get; set; }
        public float? Pf { get; set; }
        public float? Pts { get; set; }

        public virtual ICollection<FantasyTeamPlayer> FantasyTeamPlayers { get; set; }
    }
}
