using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SE_Project_Backend.models.Fantasy_Team;

namespace SE_Project_Backend.models.DTO
{
    public class PredictionDTO
    {
        public int Player_id {get; set; }
        public string Name {get; set; }
        public int? Age {get; set; }
        public float? Pts {get; set;}
        public float? GameScore {get; set; }
        public float? PredictedPoints {get; set; }
        
        public float? Fg {get; set; }
        public float? Fga {get; set; }
        public decimal? Fta {get; set; }
        public decimal? Ft {get; set; }
        public float? Orb {get; set; }
        public decimal? Drb {get; set; }
        public float? Stl {get; set; }
        public float? Ast {get; set; }
        public float? Blk {get; set; }
        public float? Pf { get; set;}
        public float? Tov {get; set;}
        

        public PredictionDTO(Player player)
        {
            this.Age = player.Age;
            this.Fg = player.Fg;
            this.Pts = player.Pts;
            this.Player_id = player.PlayerId ;
            this.Name = player.Name;
            this.Fga = player.Fga;
            this.Fta = player.Fta; 
            this.Ft = player.Ft;
            this.Orb = player.Orb;
            this.Drb = player.Drb;
            this.Stl = player.Stl;
            this.Ast = player.Ast;
            this.Blk = player.Blk;
            this.Pf = player.Pf;
            this.Tov = player.Tov;
            



//Reference for calculating game score: https://www.datarobot.com/blog/using-datarobot-to-predict-nba-player-performance/#:~:text=For%20this%20blog%2C%20I%20will,%E2%80%93%200.4%20*%20PF%20%E2%80%93%20TOV.
//The higher the game score, the better the player has been doing recently and therefore will be
//predicted to go up
                  

            if (Pts.HasValue & Fg.HasValue & Fga.HasValue & Fta.HasValue & Ft.HasValue & Orb.HasValue & Drb.HasValue & Stl.HasValue & Ast.HasValue & Blk.HasValue & Pf.HasValue) {
                this.GameScore = Pts + 0.4f * Fg - 0.7f * Fga - 0.4f * ((float)Fta - (float)Ft) + 0.7f * Orb + 0.3f * (float)Drb + Stl + 0.7f * Ast + 0.7f * Blk - 0.4f * Pf - Tov;
            } else {
                this.GameScore = 0;
            }

            if (GameScore < 1) {
                this.PredictedPoints = Pts * 0.5f;
            } else if (GameScore > 1.0f && GameScore < 3.0f){
                this.PredictedPoints = Pts * 0.65f;
            } else if (GameScore > 3.1f && GameScore < 4.0f) {
                this.PredictedPoints = Pts * 0.95f;
            } else if (GameScore > 4.1f && GameScore < 6f){
                this.PredictedPoints = Pts * 1.20f;
            } else if (GameScore > 6.1 && GameScore < 8.0f) {
                this.PredictedPoints = Pts * 1.33f;
            } else if (GameScore > 8.1 && GameScore < 10.0f) {
                this.PredictedPoints = Pts * 1.45f;
            } else if (GameScore > 10 && GameScore < 14.0f) {
                this.PredictedPoints = Pts * 1.5f;
            } else if (GameScore > 14.1 && GameScore < 18.0f) {
                this.PredictedPoints = Pts * 1.8f;
            } else if (GameScore >= 18.0f ){
                this.PredictedPoints = Pts * 2;
            }
            


            
 
            
        }
    }


}