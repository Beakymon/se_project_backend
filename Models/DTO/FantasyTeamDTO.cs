using SE_Project_Backend.models.Fantasy_Team;
using System.Collections.Generic;
namespace SE_Project_Backend.models.DTO
{
    public class FantasyTeamDTO
    {
        public string FantasyTeamId { get; set; }
        public string TeamName { get; set; }
        public decimal? FantasyScore { get; set; }
        public List<SimplePlayerDTO> Players { get; set; }
        public FantasyTeamDTO(FantasyTeam team, List<SimplePlayerDTO> players)
        {
            FantasyTeamId = team.FantasyTeamId;
            TeamName = team.TeamName;
            FantasyScore = team.FantasyScore;
            Players = players;
        }
    }
    public class SimpleFantasyTeamDTO
    {
        public string FantasyTeamId { get; set; }
        public string TeamName { get; set; }
        public SimpleFantasyTeamDTO(FantasyTeam team)
        {
            FantasyTeamId = team.FantasyTeamId;
            TeamName = team.TeamName;

        }
    }
}
