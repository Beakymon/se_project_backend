using SE_Project_Backend.models.Fantasy_Team;
namespace SE_Project_Backend.models.DTO
{
    public class SimplePlayerDTO
    {
        public int Player_id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public int? Age { get; set; }

        public SimplePlayerDTO(Player player)
        {
            this.Player_id = player.PlayerId;
            this.Name = player.Name;
            this.Position = player.Pos;
            this.Age = player.Age;
        }

    }
}