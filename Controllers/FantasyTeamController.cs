using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SE_Project_Backend.services;
using SE_Project_Backend.models.Fantasy_Team;
using SE_Project_Backend.models.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System;

namespace se_project_backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FantasyTeamController : ControllerBase
    {
        private FantasyTeamPersistenceService _context;


        public FantasyTeamController(FantasyTeamPersistenceService context)
        {
            _context = context;
        }
        //GET
        //http://localhost:5000/api/fantasyteam
        public IActionResult GetAllFantasyTeams()
        {
            var teams = _context.FantasyTeams.ToList();
            ;
            return Ok(teams.Select(x => new SimpleFantasyTeamDTO(x)));
        }
        //GET
        //http://localhost:5000/api/fantasyteam/getStats?teamId={teamId}
        [HttpGet("getStats")]
        public async Task<IActionResult> getTeam([FromQuery] string teamId)
        {
            try
            {
                var team = await _context.FantasyTeams.Where(t => t.FantasyTeamId == teamId).SingleAsync();
                if (team == null)
                    throw new Exception("Team does not exists!");

                var fantasyTeamPlayers = await _context.FantasyTeamPlayers.Where(t => t.FantasyTeamId == teamId).ToListAsync();

                //retrives players in Fantasy team
                var players = await getTeamPlayers(fantasyTeamPlayers);
                return Ok(new FantasyTeamDTO(team, players));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }

        //POST
        //http://localhost:5000/api/fantasyteam/newTeam
        [HttpPost("newTeam")]
        public async Task<IActionResult> createTeam([FromBody] FantasyTeam team)
        {
            try
            {
                if (String.IsNullOrEmpty(team.TeamName))
                {
                    throw new Exception("Invalid team name");
                }
                else if (team.TeamName.Length > 50)
                    throw new Exception("Team name is too long. Pick a name that is 50 characters or less!");
                var guid = Guid.NewGuid().ToString();
                team.FantasyTeamId = guid;
                await _context.AddAsync(team);
                await _context.SaveChangesAsync();
                return Ok(team);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }

        [HttpPost("{fantasyTeamId}/{playerId}")]
        public async Task<IActionResult> addPlayer(String fantasyTeamId, int playerId)
        {
            try
            {
                var fantasyTeam = await _context.FantasyTeams.Where(t => t.FantasyTeamId == fantasyTeamId).SingleAsync();
                if (fantasyTeam == null)
                {
                    throw new Exception("Fantasy Team does not exists");
                }
                if (await isTeamFull(fantasyTeamId))
                    throw new Exception("Fantasy team is full!");
                var players = await _context.FantasyTeamPlayers.Where(p => p.FantasyTeamId == fantasyTeamId).ToListAsync();
                //checks if it is a valid player
                var player = await _context.Players.Where(p => p.PlayerId == playerId).SingleAsync();
                if (player == null)
                    throw new Exception("Player does not exist");
                // checks if player isn't on the team 
                foreach (var p in players)
                {
                    if (p.PlayerId == playerId)
                        throw new Exception("Player is already added in the team!");
                }


                var fantasyTeamPlayer = new FantasyTeamPlayer()
                {
                    FantasyTeamId = fantasyTeam.FantasyTeamId,
                    Player = player,
                };
                await _context.FantasyTeamPlayers.AddAsync(fantasyTeamPlayer);

                await _context.SaveChangesAsync();
                var teamPlayers = await _context.FantasyTeamPlayers.Where(t => t.FantasyTeamId == fantasyTeam.FantasyTeamId).ToListAsync();
                if (teamPlayers.Count == 5)
                {
                    fantasyTeam.FantasyScore = Decimal.Round(generateFantasyScore(), 1);
                }
                await _context.SaveChangesAsync();
                return Ok(new FantasyTeamDTO(fantasyTeam, await getTeamPlayers(teamPlayers)));

            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
        [HttpDelete("RemovePlayer")]
        public async Task<IActionResult> RemovePlayer(String teamId, int playerId)
        {
            try
            {

                var teamPlayer = await _context.FantasyTeamPlayers.Where(x => x.FantasyTeamId == teamId && x.PlayerId == playerId).SingleAsync();
                if (teamPlayer == null)
                    throw new Exception("Player/Team does not exists");
                _context.FantasyTeamPlayers.Remove(teamPlayer);
                await _context.SaveChangesAsync();
                //remove fantasy score since team is not complete
                var team = await _context.FantasyTeams.Where(x => x.FantasyTeamId == teamId).SingleAsync();
                team.FantasyScore = null;
                await _context.SaveChangesAsync();
                var teamPlayers = await _context.FantasyTeamPlayers.Where(x => x.FantasyTeamId == teamId).ToListAsync();
                return Ok(await getTeamPlayers(teamPlayers));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }
        [HttpGet("GetPlayers")]
        public async Task<IActionResult> getPlayersFromTeam([FromQuery] string teamId)
        {
            var playersInTeam = await _context.FantasyTeamPlayers.Where(x => x.FantasyTeamId == teamId).ToListAsync();
            return Ok(await getTeamPlayers(playersInTeam));
        }
        private decimal generateFantasyScore()
        {
            //TEMP!!
            // will be replaced when prediction class is created
            Random random = new Random();
            return ((decimal)random.NextDouble() * (99 - 0) + 0);
        }

        //checks if team is full
        private async Task<bool> isTeamFull(string teamId)
        {
            var players = await _context.FantasyTeamPlayers.Where(p => p.FantasyTeamId == teamId).ToListAsync();

            //check if team has space
            if (players.Count >= 5)
            {
                return true;
            }
            return false;
        }

        //returns a lsit of players from fantasy team
        private async Task<List<SimplePlayerDTO>> getTeamPlayers(List<FantasyTeamPlayer> teamPlayers)
        {
            var players = new List<SimplePlayerDTO>();
            foreach (var p in teamPlayers)
            {
                var player = await _context.Players.Where(t => t.PlayerId == p.PlayerId).SingleAsync();
                players.Add(new SimplePlayerDTO(player));
            }
            return players;
        }
    }
}