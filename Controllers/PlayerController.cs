using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SE_Project_Backend.models.Fantasy_Team;
using SE_Project_Backend.services;
using SE_Project_Backend.models.DTO;

namespace se_project_backend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlayerController : ControllerBase
    {

        private NBAPredictorContext _context;

        public PlayerController(NBAPredictorContext context)
        {
            _context = context;
        }

        public ActionResult<List<Player>> GetAllPlayers()
        {
            return _context.Players.ToList();
        }

        [HttpGet("getplayer/{id}")]
        public ActionResult<Player> GetPlayer(int id)
        {
            return _context.Players.Where(p => p.PlayerId.Equals(id)).Single();
        }

        [HttpGet("searchplayer/{name}")]
        public ActionResult<List<Player>> SearchPlayer(string name)
        {
            return _context.Players.Where(player => player.Name.ToLower().Contains(name.ToLower())).ToList();
        }

        [HttpGet("playerlist")]

        public ActionResult<List<SimplePlayerDTO>> getPlayerList()
        {
            var players = _context.Players.ToList();
            var playersList = new List<SimplePlayerDTO>();

            foreach (var p in players)
            {
                playersList.Add(new SimplePlayerDTO(p));
            }
            return Ok(playersList);
        }

               //lists every player and their prediction
        [HttpGet("predictallplayers")]
        public ActionResult<List<PredictionDTO>> predictAllPlayers(){

            var players = _context.Players.ToList();
            var playersList = new List<PredictionDTO>();

            
            foreach (var p in players)
            {
                playersList.Add(new PredictionDTO(p));
            }

            return Ok(playersList);
        }

        //enter a players name and get a prediction for that player
    
        [HttpGet("getPrediction/{name}")]
        public IActionResult getPrediction(string name) {

            return Ok(new PredictionDTO(_context.Players.Where( x=> x.Name == name).First()));
        }

    }
}